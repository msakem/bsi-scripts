﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Text;

using System.Windows.Forms;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.IE;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using System.IO;

namespace UnitTestBsiScripts
{
    [TestFixture]
    public class UntitledTestCase
    {
        private InternetExplorerDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;

        [SetUp]
        public void SetupTest()
        {
            verificationErrors = new StringBuilder();
            /*
            Proxy proxy = new Proxy { HttpProxy = "inetproxy:83", SslProxy = "inetproxy:83", Kind = ProxyKind.Manual, IsAutoDetect = false };

            var options = new InternetExplorerOptions
            {

                //UsePerProcessProxy = true,
                Proxy = proxy,
                IntroduceInstabilityByIgnoringProtectedModeSettings = true,
                IgnoreZoomLevel = true,
                EnableNativeEvents = false
            };
            */


            driver = new OpenQA.Selenium.IE.InternetExplorerDriver();
            driver.Manage().Window.Maximize();
            baseURL = "https://www.katalon.com/";
            verificationErrors = new StringBuilder();
        }

        [TearDown]
        public void TeardownTest()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            NUnit.Framework.Assert.AreEqual("", verificationErrors.ToString());
        }
        /*
        #region Administrateur
        
        /// <summary>
        /// Administrateurs=>Créer un administrateur
        /// </summary>
        [Test]
        public void BSI_EX004()
        {
            if (Authenticate())
            {
                driver.Navigate().GoToUrl("https://acc.es2h.pp.s2h.corp/fr/");
                driver.FindElement(By.CssSelector("div.menu_item_w:nth-child(2) > a:nth-child(1)")).Click();

                driver.FindElement(By.CssSelector("html body div.content_w div.content div.crud_toolbar a.action_button:nth-child(4)")).Click();

                driver.FindElement(By.Id("acc_administrator_firstName")).Click();

                driver.FindElement(By.Id("acc_administrator_firstName")).SendKeys("DODO");
                driver.FindElement(By.Id("acc_administrator_lastName")).Click();

                driver.FindElement(By.Id("acc_administrator_lastName")).SendKeys("DODI");
                driver.FindElement(By.Id("acc_administrator_email")).Click();

                driver.FindElement(By.Id("acc_administrator_email")).SendKeys("issam.lemlih@gmail.com");
                driver.FindElement(By.Id("acc_administrator_roles_1")).Click();
                driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Administrateur'])[1]/following::button[1]")).Click();
                Thread.Sleep(2000);
            }
        }
        
        /// <summary>
        /// Administrateurs=>Créer des administrateurs
        /// </summary>
        [Test]
        public void BSI_EX0040()
        {
            if (Authenticate())
            {
                driver.Navigate().GoToUrl("https://acc.es2h.pp.s2h.corp/fr/");
                driver.FindElement(By.CssSelector("div.menu_item_w:nth-child(2) > a:nth-child(1)")).Click();
                string csvFile = @"C:\Program Files (x86)\Jenkins\workspace\bsi_scripts\UnitTestBsiScripts\Data\DataAdministrato.csv";


                string[] lines = File.ReadAllLines(csvFile);
                foreach (string line in lines)
                {
                    string[] data = line.Split();

                    driver.FindElement(By.CssSelector("html body div.content_w div.content div.crud_toolbar a.action_button:nth-child(4)")).Click();

                    driver.FindElement(By.Id("acc_administrator_firstName")).Click();

                    driver.FindElement(By.Id("acc_administrator_firstName")).SendKeys(data[0]);
                    driver.FindElement(By.Id("acc_administrator_lastName")).Click();

                    driver.FindElement(By.Id("acc_administrator_lastName")).SendKeys(data[1]);
                    driver.FindElement(By.Id("acc_administrator_email")).Click();

                    driver.FindElement(By.Id("acc_administrator_email")).SendKeys(data[2]);
                    if (data[3] == "Administrateur")
                    {
                        driver.FindElement(By.Id("acc_administrator_roles_1")).Click();
                    }
                    else
                    {
                        driver.FindElement(By.Id("acc_administrator_roles_0")).Click();
                    }

                    driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Administrateur'])[1]/following::button[1]")).Click();
                    Thread.Sleep(2000);
                    driver.Navigate().GoToUrl("https://acc.es2h.pp.s2h.corp/fr/");
                    driver.FindElement(By.CssSelector("div.menu_item_w:nth-child(2) > a:nth-child(1)")).Click();


                }
            }
        }
        
        /// <summary>
        /// Administrateur=>Supprimer l'administrateur sélectionné
        /// </summary>
        [Test]
        public void BSI_EX00020()
        {
            if (Authenticate())
            {
                driver.Navigate().GoToUrl("https://acc.es2h.pp.s2h.corp/fr/");
                driver.FindElement(By.CssSelector("div.menu_item_w:nth-child(2) > a:nth-child(1)")).Click();
                driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Administrateur'])[3]/following::a[3]")).Click();
                driver.FindElement(By.CssSelector("#modal_crud_confirm > div.modal_crud.modal_confirm > div.form_button_row > a.form_button.error_button")).Click();
            }
        }
        /// <summary>
        /// Administrateur=>Modifier l'administrateur sélectionné
        /// </summary>
        [Test]
        public void BSI_EX000200()
        {
            if (Authenticate())
            {
                driver.Navigate().GoToUrl("https://acc.es2h.pp.s2h.corp/fr/");
                driver.FindElement(By.CssSelector("div.menu_item_w:nth-child(2) > a:nth-child(1)")).Click();
                driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Administrateur'])[3]/following::a[2]")).Click();
                driver.FindElement(By.Id("acc_administrator_firstName")).Click();
                driver.FindElement(By.Id("acc_administrator_firstName")).Clear();
                driver.FindElement(By.Id("acc_administrator_firstName")).SendKeys("DODOH");
                driver.FindElement(By.Id("acc_administrator_lastName")).Click();
                driver.FindElement(By.Id("acc_administrator_lastName")).Clear();
                driver.FindElement(By.Id("acc_administrator_lastName")).SendKeys("DODIH");
                driver.FindElement(By.Id("acc_administrator_email")).Click();
                driver.FindElement(By.Id("acc_administrator_email")).Clear();
                driver.FindElement(By.Id("acc_administrator_email")).SendKeys("issam.lemlihH@gmail.com");
                driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Administrateur'])[1]/following::button[1]")).Click();

            }
        }
        /// <summary>
        /// Administrateur=>désactiver l'administrateur sélectionné
        /// </summary>
        [Test]
        public void BSI_EX0002000()
        {
            if (Authenticate())
            {
                driver.Navigate().GoToUrl("https://acc.es2h.pp.s2h.corp/fr/");
                driver.FindElement(By.CssSelector("div.menu_item_w:nth-child(2) > a:nth-child(1)")).Click();
                driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Administrateur'])[3]/following::a[4]")).Click();

            }
        }
        /// <summary>
        /// Administrateur=>Activer l'administrateur sélectionné
        /// </summary>
        [Test]
        public void BSI_EX00020000()
        {
            if (Authenticate())
            {
                driver.Navigate().GoToUrl("https://acc.es2h.pp.s2h.corp/fr/");
                driver.FindElement(By.CssSelector("div.menu_item_w:nth-child(2) > a:nth-child(1)")).Click();
                driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Administrateur'])[3]/following::a[4]")).Click();

            }
        }
        #endregion
        */
        /*
        #region Entreprises
        /// <summary>
        /// Entreprises=>Créer une entreprise
        /// </summary>
        [Test]
        public void BSI_EX000200000()
        {
            if (Authenticate())
            {
                driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Collaborateurs'])[1]/following::span[2]")).Click();
                driver.FindElement(By.LinkText("Clients")).Click();
                driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Liste des clients'])[2]/following::span[1]")).Click();
                driver.FindElement(By.Id("employer_name")).Click();
                driver.FindElement(By.Id("employer_name")).Clear();
                driver.FindElement(By.Id("employer_name")).SendKeys("clientH1");
                driver.FindElement(By.Id("employer_url")).Click();
                driver.FindElement(By.Id("employer_url")).Clear();
                driver.FindElement(By.Id("employer_url")).SendKeys("clienth");
                driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Langue'])[1]/following::input[1]")).Click();
                driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Langue'])[1]/following::input[1]")).Click();
                driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Langue'])[1]/following::input[1]")).Click();
                // ERROR: Caught exception [ERROR: Unsupported command [doubleClick | xpath=(.//*[normalize-space(text()) and normalize-space(.)='Langue'])[1]/following::input[1] | ]]
                driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Français'])[1]/following::input[1]")).Click();
                driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Anglais'])[2]/following::div[1]")).Click();
                driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Type 2ème identifiant'])[1]/following::input[1]")).Click();
                driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Choisir'])[2]/following::div[1]")).Click();
                IWebElement element = driver.FindElement(By.Id("employer_logo_uploadedFile"));
              // String filePath = @"C:\Users\hmsakem\Source\Repos\bsi_scripts\UnitTestBsiScripts\Images\nestle_logo_3.jpeg";
            String filePath = @"C:\Program Files (x86)\Jenkins\workspace\bsi_scripts\UnitTestBsiScripts\Images\nestle_logo_3.jpeg";
            element.SendKeys(filePath);
                System.Windows.Forms.SendKeys.SendWait("{ENTER}");
                System.Threading.Thread.Sleep(4000);
                driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Dimension: 86px de hauteur'])[1]/following::button[1]")).Click();
            }
        }
        #endregion
        */

        #region Tableau de bord
        /// Tableau de bord=>Vider le cache
        /// </summary>
        [Test]
        public void BSI_EX0001()
        {
            if (Authenticate())
            {
                driver.FindElement(By.CssSelector("div.menu_item_w:nth-child(1)")).Click();
                driver.FindElement(By.CssSelector("div.form_button_row:nth-child(1) > a:nth-child(1)")).Click();
                Thread.Sleep(2000);
            }
        }
        /// <summary>
        /// Tableau de bord=>Voir toutes les entreprises
        /// </summary>
        [Test]
        public void BSI_EX0002()
        {
            if (Authenticate())
            {
                driver.FindElement(By.CssSelector("div.menu_item_w:nth-child(1)")).Click();
                driver.FindElement(By.CssSelector("html body div.content:nth-child(3) > div:nth-child(3) > a:nth-child(1)")).Click();
                Thread.Sleep(2000);
            }
        }
        /// <summary>
        /// Tableau de bord=>Modifier Une entreprise sans modifier son logo
        /// </summary>
        [Test]
        public void BSI_EX0003()
        {
            if (Authenticate())
            {

                driver.FindElement(By.CssSelector("div.menu_item_w:nth-child(1)")).Click();
                driver.FindElement(By.XPath("(//a[contains(@href, '/fr/company/39/edit.html')])[3]")).Click();
                driver.FindElement(By.Id("employer_name")).Click();
                driver.FindElement(By.Id("employer_name")).Clear();
                driver.FindElement(By.Id("employer_name")).SendKeys("testM");
                driver.FindElement(By.Id("employer_url")).Click();
                driver.FindElement(By.Id("employer_url")).Clear();
                driver.FindElement(By.Id("employer_url")).SendKeys("testm");
                driver.FindElement(By.XPath("/html[1]/body[1]/div[3]/div[2]/form[1]/div[1]/div[3]/div[2]/input[1]")).Click();
                driver.FindElement(By.CssSelector("div.custom_select_w:nth-child(3) > div:nth-child(3) > div:nth-child(1)")).Click();
                driver.FindElement(By.XPath("/html[1]/body[1]/div[3]/div[2]/form[1]/div[1]/div[3]/div[2]/input[1]")).Click();
                driver.FindElement(By.CssSelector("div.custom_select_w:nth-child(3) > div:nth-child(3) > div:nth-child(3)")).Click();
                driver.FindElement(By.CssSelector("div.custom_select_w:nth-child(2) > input:nth-child(2)")).Click();
                driver.FindElement(By.CssSelector("div.custom_select_w:nth-child(2) > div:nth-child(3) > div:nth-child(2)")).Click();
                driver.FindElement(By.CssSelector("button.form_button")).Click();
            }
        }
        #endregion

        private bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        private bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        private string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
                acceptNextAlert = true;
            }
        }

        bool Authenticate()
        {
            try
            {
                driver.Navigate().GoToUrl("https://acc.es2h.pp.s2h.corp/fr/login.html");
                driver.Navigate().GoToUrl("javascript:document.getElementById('overridelink').click()");
                driver.FindElement(By.Id("_username")).Click();
                driver.FindElement(By.Id("_username")).SendKeys("Houda.MSAKEM@s2hgroup.com");
                driver.FindElement(By.Id("_password")).Click();
                driver.FindElement(By.Id("_password")).SendKeys("Bonbon000@");
                driver.FindElement(By.Id("authenticate")).Click();
                driver.Navigate().GoToUrl("https://acc.es2h.pp.s2h.corp/fr/");
                return true;

            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
